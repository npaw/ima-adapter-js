/* global google */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Ima = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = this.playhead
    if (!this.flags.isJoined) {
      ret = 0
    } else if (this.player && !this.isDAI) {
      ret = this.getDuration() - this.player.getRemainingTime()
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = this.duration
    if (this.player && !this.isDAI) {
      if (this.player.getCurrentAd && this.player.getCurrentAd()) {
        this.lastDuration = this.player.getCurrentAd().getDuration()
      }
      ret = this.lastDuration
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    if (this.player) {
      if (!this.isDAI) {
        var currentAd = this.player.getCurrentAd()
        ret = currentAd.getVastMediaWidth().toString() + 'x' + currentAd.getVastMediaHeight().toString()
      } else if (this.width && this.height) {
        ret = this.width + 'x' + this.height
      }
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    var ret = this.title
    if (this.player && !this.isDAI) {
      ret = this.player.getCurrentAd().getTitle()
    }
    return ret
  },

  /** Override to return title2 */
  getTitle2: function () {
    var ret = this.advertiser
    if (this.player && !this.isDAI) {
      ret = this.player.getCurrentAd().getAdvertiserName()
    }
    return ret
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = null
    if (this.player && !this.isDAI) {
      ret = this.player.getCurrentAd().getMediaUrl()
    }
    return ret
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return google.ima.VERSION
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'IMA'
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.player && this.player.getCuePoints) {
      ret = this.player.getCuePoints().length
    }
    return ret
  },

  getBreaksTime: function () {
    var cuepoints = null
    if (this.player && this.player.getCuePoints) {
      cuepoints = this.player.getCuePoints()
      cuepoints.forEach(function (value, index) {
        if (cuepoints[index] === -1 || cuepoints[index] === null || cuepoints[index] === undefined) {
          cuepoints[index] = this.plugin._adapter.getDuration()
        }
      }.bind(this))
    }
    return cuepoints
  },

  getGivenAds: function () {
    return this.totalAds
  },

  getAudioEnabled: function () {
    var ret = null
    if (this.player) {
      ret = this.player.getVolume ? this.player.getVolume() !== 0 : true
    }
    return ret
  },

  getIsSkippable: function () {
    var ret = null
    if (this.player && this.player.getCurrentAd) {
      ret = this.player.getCurrentAd().g.skippable
    }
    return ret
  },

  /*
  // Not reliable
  getIsFullscreen: function () {
    var ret = false
    if (this.player) {
      if (!this.contentPlayer) {
        for (var key in this.player) {
          var element = this.player[key]
          if (!!element && element.videoHeight && element.clientHeight) {
            this.contentPlayer = element
            break
          }
        }
      }
      if (this.contentPlayer && this.contentPlayer.clientWidth && this.contentPlayer.clientHeight) {
        ret = (window.innerHeight <= this.contentPlayer.clientHeight + 30 && window.innerWidth <= this.contentPlayer.clientWidth + 30)
      }
    }
    return ret
  }, */

  getIsVisible: function () {
    var ret = null
    if (this.player) {
      if (!this.contentPlayer) {
        for (var key in this.player) {
          var element = this.player[key]
          if (!!element && element.videoHeight && element.clientHeight) {
            this.contentPlayer = element
            break
          }
        }
      }
      ret = youbora.Util.calculateAdViewability(this.contentPlayer)
    }
    return ret
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    var ret = null
    if (this.player) {
      if (this.plugin.getAdapter() && !this.plugin.getAdapter().flags.isJoined) {
        ret = youbora.Constants.AdPosition.Preroll
      } else if (this.plugin.getIsLive()) {
        ret = youbora.Constants.AdPosition.Midroll
      } else {
        ret = this.adPosition || youbora.Constants.AdPosition.Midroll
      }
    }
    return ret
  },

  getCreativeId: function () {
    var ret = null
    if (this.player && this.player.getCurrentAd) {
      ret = this.player.getCurrentAd().g.creativeId
    }
    return ret
  },

  getAdInsertionType: function () {
   return this.isDAI ? youbora.Constants.AdInsertionType.ServerSide : youbora.Constants.AdInsertionType.ClientSide
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // References
    this.references = {}
    if (google.ima.AdEvent) {
      this.monitorPlayhead(true, false)
      var event = google.ima.AdEvent.Type
      // Console all events if logLevel=DEBUG
      // youbora.Util.logAllEvents(this.player.addEventListener, [null, event.LOADED, event.CONTENT_PAUSE_REQUESTED])

      this.references[event.LOADED] = this.readyListener.bind(this)
      this.references[event.CONTENT_PAUSE_REQUESTED] = this.playListener.bind(this)
      this.references[event.LOADED] = this.loadedListener.bind(this)
      this.references[event.PAUSED] = this.pauseListener.bind(this)
      this.references[event.STARTED] = this.playingListener.bind(this)
      this.references[event.RESUMED] = this.playingListener.bind(this)
      this.references[google.ima.AdErrorEvent.Type.AD_ERROR] = this.errorListener.bind(this)
      this.references[event.COMPLETE] = this.endedListener.bind(this)
      this.references[event.CONTENT_RESUME_REQUESTED] = this.endedListener.bind(this)
      this.references[event.SKIPPED] = this.skippedListener.bind(this)
      this.references[event.CLICK] = this.clickListener.bind(this)
      this.references[event.ALL_ADS_COMPLETED] = this.endedViewListener.bind(this)
      this.references[event.LOG] = this.logListener.bind(this)
      this.references[event.FIRST_QUARTILE] = this.firstQuartileListener.bind(this)
      this.references[event.MIDPOINT] = this.secondQuartileListener.bind(this)
      this.references[event.THIRD_QUARTILE] = this.thirdQuartileListener.bind(this)
      this.references[event.CONTENT_RESUME_REQUESTED] = this.breakEndListener.bind(this)
    }
    // DAI
    if (google.ima.dai) {
      var eventDAI = google.ima.dai.api.StreamEvent.Type
      this.references[eventDAI.SKIPPED] = this.skippedL.bind(this)
      this.references[eventDAI.ERROR] = this.errorDAIListener.bind(this)
      this.references[eventDAI.AD_BREAK_STARTED] = this.startDAIListener.bind(this)
      this.references[eventDAI.AD_BREAK_ENDED] = this.stopDAIListener.bind(this)
      this.references[eventDAI.AD_PROGRESS] = this.progressDAIListener.bind(this)
      this.references[eventDAI.CLICK] = this.clickDAIListener.bind(this)
      // this.references[eventDAI.COMPLETE] = this.stopDAIListener.bind(this)
      this.references[eventDAI.STARTED] = this.joinDAIListener.bind(this)
      this.references[eventDAI.FIRST_QUARTILE] = this.firstQuartileListener.bind(this)
      this.references[eventDAI.MIDPOINT] = this.secondQuartileListener.bind(this)
      this.references[eventDAI.THIRD_QUARTILE] = this.thirdQuartileListener.bind(this)
    }
    // Register listeners
    if (this.player) {
      for (var key in this.references) {
        this.player.addEventListener(key, this.references[key])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    if (this.monitor) this.monitor.stop()
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = {}
    }
  },

  skippedL: function () {
    this.fireSkip()
  },

  /** DAI **/
  errorDAIListener: function (e) {
    this.fireError()
    this.fireStop()
  },

  startDAIListener: function (e) {
    this.isDAI = true
    this.playhead = 0
    this.adPosition = this.getAdPosition()
    this.duration = null
    this.plugin.fireInit() // if preroll
    this.fireStart()
  },

  progressDAIListener: function (e) {
    if (e.getStreamData && e.getStreamData()) {
      this.playhead = e.getStreamData().adProgressData.currentTime
    } else if (typeof e.getAdData === 'function' && e.getAdData()) {
      this.playhead = e.getAdData().currentTime
    }
    this.fireJoin()
  },

  clickDAIListener: function (e) {
    this.fireClick(this.clickUrl)
  },

  stopDAIListener: function (e) {
    this.fireStop({ adPlayhead: this.duration })
    if (this.plugin.getAdapter()) {
      this.plugin.getAdapter().fireResume()
    }
    this.fireBreakStop()
  },

  joinDAIListener: function (e) {
    var adData = e.getAd()
    if (adData) {
      this.duration = adData.getDuration()
      this.title = adData.getTitle()
      this.width = adData.getVastMediaWidth()
      this.height = adData.getVastMediaHeight()
      this.advertiser = adData.getAdvertiserName()
    }
    if (this.plugin.getAdapter()) this.plugin.getAdapter().firePause()
    this.plugin.fireInit() // if preroll
    this.fireStart()
    this.fireJoin()
  },

  logListener: function (e) {
    if (typeof e.getAdData === 'function' && e.getAdData().adError) {
      var error = e.getAdData().adError
      this.fireError(error.getErrorCode(), error.getMessage())
      this.fireStop()
    }
  },

  loadedListener: function (e) {
    var adData = typeof e.getAdData === 'function' ? e.getAdData() : null
    this.totalAds = (adData && adData.adPodInfo) ? adData.adPodInfo.totalAds : null
    if (this.plugin.isBreakStarted) {
      this.playListener(e)
    }
  },

  /** Listener for 'play' and 'content_pause_requested' events. */
  playListener: function (e) {
    console.log("JOSEP BERNAD");
    debugger;
    this.isDAI = false
    this.adPosition = this.getAdPosition()
    var adData = typeof e.getAdData === 'function' ? e.getAdData() : null
    this.totalAds = (adData && adData.adPodInfo) ? adData.adPodInfo.totalAds : null
    this.plugin.fireInit() // if preroll
    if (this.plugin.getAdapter()) {
      this.plugin.getAdapter().firePause()
    }
    this.fireStart({ adPlayhead: '0' })
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' and 'resume' events. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin({ adPlayhead: '0' })
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError()
    this.fireStop()
  },

  firstQuartileListener: function (e) {
    this.fireQuartile(1)
  },

  secondQuartileListener: function (e) {
    this.fireQuartile(2)
  },

  thirdQuartileListener: function (e) {
    this.fireQuartile(3)
  },

  /** Listener for 'ended' and 'content_resume_requested' events. */
  endedListener: function (e) {
    if (this.lastDuration) {
      this.fireStop({ adPlayhead: this.lastDuration })
    } else {
      this.fireStop()
    }

  },

  /** Listener for 'skipped' event. */
  skippedListener: function (e) {
    this.fireSkip({ adPlayhead: this.getPlayhead() })
  },

  /** Listener for 'click' event. */
  clickListener: function (e) {
    var url = this.clickUrl
    var current = this.player.getCurrentAd()
    if (current && current.g && current.g.clickThroughUrl){
      url = current.g.clickThroughUrl
    }
    var now = new Date().getTime()
    if (this.lastUrl === url && now < ((this.lastTime || 0) + 2000)) {
      return
    }
    this.lastUrl = url
    this.lastTime = now
    this.fireClick(url)
  },

  /** Listener for 'allAdsCompleted' event. */
  endedViewListener: function (e) {
    if (this.getPosition() === youbora.Constants.AdPosition.Postroll) {
      this.plugin.fireStop()
    }
  },

  readyListener: function (e) {
    var adData = typeof e.getAdData === 'function' ? e.getAdData() : null
    if (adData) {
      if (adData.adPodInfo) {
        this.totalAds = adData.adPodInfo.totalAds
      }
      this.clickUrl = adData.clickThroughUrl
    }
  },

  breakEndListener: function (e) {
    this.fireBreakStop()
  },

  getAdPosition: function () {
    var ret = youbora.Constants.AdPosition.Midroll
    if (!this.isDAI) {
      switch (this.player.getCurrentAd().getAdPodInfo().getTimeOffset()) {
        case 0:
          ret = youbora.Constants.AdPosition.Preroll
          break
        case -1:
          ret = youbora.Constants.AdPosition.Postroll
      }
    } else {
      if (this.plugin.getAdapter() &&
      (!this.plugin.getAdapter().flags.isJoined || this.plugin.getAdapter().getPlayhead() < 1)) {
        ret = youbora.Constants.AdPosition.Preroll
      }
    }
    return ret
  }
})

module.exports = youbora.adapters.Ima
