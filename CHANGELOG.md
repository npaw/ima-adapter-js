## [6.8.3] - 2024-09-26
### Fixed
- Midroll position constant.
### Library
- Packaged with `lib 6.8.57`.

## [6.8.2] - 2022-05-31
### Added
- Edge case check where no prerolls were reported for DAI.

## [6.8.1] - 2022-01-04
### Removed
- Is fullscreen detection, not reliable

## [6.8.0] - 2021-12-03
### Library
- Packaged with `lib 6.8.9`

## [6.7.6] - 2021-07-15
### Added
- Safe access to `getAdData` method

## [6.7.5] - 2021-07-14
### Added
- Stop adPlayhead fix for some cases

## [6.7.4] - 2021-05-26
### Added
- Insertion type (clientside, serverside)
### Library
- Packaged with `lib 6.7.34`

## [6.7.3] - 2020-09-08
### Fixed
- All the ads when joined and live are reported as midrolls

## [6.7.2] - 2020-08-28
### Fixed
- Adposition detection for DAI when isLive is reported as an option and not coming from the player.

## [6.7.1] - 2020-06-09
### Fixed
- Adclick not reported always, added alternative solution

## [6.7.0] - 2020-05-27
### Added
- Code refactor
### Fixed
- Check possible null references if player is not set
- Reporting the beginning of the breaks before they actually start
### Library
- Packaged with `lib 6.7.7`

## [6.5.4] - 2019-11-20
### Library
- Packaged with `lib 6.5.20`

## [6.5.3] - 2019-11-15
### Fixed
- Tracking consecutive ads for DAI
### Library
- Packaged with `lib 6.5.19`
- Improved build size

## [6.5.2] - 2019-10-11
### Fixed
- Error instead of adskip being sent for DAI
- Quartiles and Jointime not working after skipped ad

## [6.5.1] - 2019-10-03
### Fixed
- Playhead not detected always for DAI
- Listeners for quartiles triggered twice

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.4.4] - 2019-02-07
### Fixed
- Blocked double adclick being sent, increased threshold to 2 seconds
- Ad error before jointime reported as midroll

## [6.4.3] - 2019-02-06
### Fixed
- Blocked double adclick being sent
- Errors not being reported

## [6.4.2] - 2019-02-04
### Fixed
- Case blocking stop request when having no postrolls

## [6.4.1] - 2018-11-22
### Fixed
- Removed monitor.stop bug

## [6.4.0] - 2018-09-25
### Added
- Stop call after error
### Library
- Packaged with `lib 6.4.8`

## [6.2.2] - 2018-05-24
### Fixed
- Minor updates for DAI with live content

## [6.2.1] - 2018-05-03
### Added
- Support for DAI

## [6.2.0] - 2018-04-23
### Library
- Packaged with `lib 6.2.0`
